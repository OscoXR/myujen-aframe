/* global AFRAME */
var dialogueTextEl;
var helpMenuActive = true;
var index = 0;
var lines;
var speakerPlateEl;
var speakerTextEl;
var numberOfLines = 45; // TODO: Update in Init via REST

// Get Dialogue from Line Index
function getDialogue(index) {
  return lines[index].dialogue
}

// Get Speaker from Line Index
function getSpeaker(index) {
  return lines[index].speaker
}

// Shows or Hides Speaker Plate
function setSpeakerPlateVisibility(speaker) {
  if (speaker){
    speakerPlateEl.object3D.visible = true;
  }
  else {
    speakerPlateEl.object3D.visible = false;
  }
}

AFRAME.registerComponent('component-dialogue', {
  init: function () {

    // Add Event Listen for Keystokes and Parse in onKeydown()
    window.addEventListener('keydown', this.onKeydown);

    // Import Dialogue from REST Call via Window Id Params
    // TODO: Invoke REST call and parse data into global vars
    // TODO: Remove this nonsense lol
    // TODO: You can prob fake the lines for the demo :) 
    // lines = [
    //   {"speaker": "Me", "dialogue": "\"What a gorgeous day outside!\""},
    //   {"speaker": "", "dialogue": "\"You find yourself stretching after a long day in class.\""},
    //   {"speaker": "Me", "dialogue": "\"Man, what a bore! But now it's all over, which means I can go home and start listening to hip-hop!\""},
    //   {"speaker": "", "dialogue": "\"You begin walking home throught market street like you always do, you today you can't help but notice an alleyway that wasn't always there.\""},
    //   {"speaker": "Me", "dialogue": "\"Huh, isn't that weird? I've walked down this street hundreds of times, but I haven't seen this before.\""},
    //   {"speaker": "Me", "dialogue": "\"It's weirdly convenient too, it passes through the market district straight to my neighborhood without having to go around!\""},
    //   {"speaker": "", "dialogue": "Lucky! Looks like you'll get home in no time.\""},
    //   {"speaker": "", "dialogue": "\"You start moving down the alley, when suddenly a shadow darts from the corner of your eye.\""},
    //   {"speaker": "Me", "dialogue": "\"He-\","},
    //   {"speaker": "", "dialogue": "\"You begin to say, when an insane pain slams into the back of your head.\""},
    //   {"speaker": "Me", "dialogue": "\"Urgh...\""},
    //   {"speaker": "", "dialogue": "\"You are unable to finish your sentence as you begin tumbling into the ground as your vision fades to black.\""},
    // ]
    lines = [
      {speaker: "", dialogue: "<TEST LINE ONLY>"},
      {speaker: "", dialogue: "It's been a long drive to your new workplace. Pulling through the long dusty road covered by a treetop canopy, the forest line clears and you see it in the distance... the academy!"},
      {speaker: "", dialogue: "The academy has long rolling grounds tended carefully to give the a pristine and refined appearance. Several facilities inbahit the edges of the campus, from various sports fields to storage units and even housing for the students and professors."},
      {speaker: "", dialogue: "As you drive closer to the academy, you start to see signs of life. But... what are they?"},
      {speaker: "", dialogue: "Expecting to see normal kids and faculty walking thoughout the school grounds, you instead see various shapes and sizes of not-quite-human figures."},
      {speaker: "", dialogue: "Part of you wonders why everyone is dressed this way. Early Halloween? Cosplay? Maybe some special Renaissance fair event?"},
      {speaker: "", dialogue: "Shaking off the intrusive thoughts, you pull into the mostly-empty parking lot and begin to get out of your car."},
      {speaker: "Me", dialogue: "\"Eek!\""},
      {speaker: "", dialogue: "As you step out of your car, you foot gets caught in a tiny pothole and you stumble over onto the parking lot."},
      {speaker: "Me", dialogue: "\"A-cha-cha-cha...\""},
      {speaker: "", dialogue: "You land on your hands and scrape your knees roughly. As you slowly pull your foot out of the pothole and begin rubbing your knees to shake the asphault off, a large shadow looms over you."},
      {speaker: "???", dialogue: "\"Hoo-HOO!\""},
      {speaker: "", dialogue: "What sounds like an owl purring above you, you finish standing up and tilt you head upwards to find the source of the noise."},
      {speaker: "???", dialogue: "\"Oh, it's you! Welcome to the academy, professor!\""},
      {speaker: "", dialogue: "Now staring at the source of the noise, you come face to face with a giant owl man."},
      {speaker: "Dev Team", dialogue: "[We're still drawing the owl please just imagine an old stogy owl man heehee]"},
      {speaker: "Me", dialogue: "\"Hu-huh??\""},
      {speaker: "", dialogue: "You're left speechless as something beyond imagination stands before you in reality."},
      {speaker: "???", dialogue: "\"What's wrong, professor? You look like you've seen a human!\""},
      {speaker: "", dialogue: "You're still left speechless as you process the current situation and also what the owl man just said."},
      {speaker: "???", dialogue: "\"Oh I'm sorry, I should have introduced myself. I'm the Headmaster, Octavio Wincester Leorio, but the faculty here just calls me Owl! A pleasure to meet you!\""},
      {speaker: "", dialogue: "The owl-man named Owl extends out one of his wings for a handshake, which also ends in a human hand shockingly."},
      {speaker: "", dialogue: "Still in mental shell shock, you body instinctively extends your hand out to return the handshake, and Owl smiles. "},
      {speaker: "Me", dialogue: "\"Aah!\""},
      {speaker: "", dialogue: "You start to stumble backwards as more stimulation overenergizes your body, but before you hit the ground, you feel a soft but strong pair of hands catch you."},
      {speaker: "???", dialogue: "\"That was a close one, professor! It would really be a shame if you got hurt as soon as you got to grounds!\""},
      {speaker: "", dialogue: "As your body braced for impact into the concrete once more, they instinctively closed their eyes. Opening them slowly, you start to see the outline of a man looking directly at you."},
      {speaker: "", dialogue: "Thank goodness, I must have been imagining the owl ma-"},
      {speaker: "", dialogue: "Your train of though ends abruptly as the ouline of the man holding you begins to make itself clear, and you notice that they have blue skin... and wings?"},
      {speaker: "", dialogue: "Before your voice give out a chance to scream, you stop."},
      {speaker: "", dialogue: "The man... the man...? The moth man...? Is actually really attractive."},
      {speaker: "", dialogue: "Your nerves begin to quell, but then your heart rate starts to elevate without your consent."},
      {speaker: "???", dialogue: "\"Sorry to catching you without any warning, professor, I hope that's okay!\""},
      {speaker: "", dialogue: "The moth man gingerly sets you upright to make sure you can stand, and then begins smiling warmly after confirming your safety."},
      {speaker: "Mothman", dialogue: "\"I was hoping to introduce myself more casually, but it's better late then never! My name is Matteo Orlando Tomas Henderson, or Mothman as the rest of the facultly calls me. It is a pleasure to meet you.\""},
      {speaker: "", dialogue: "The mothman's warm smile and gently gaze puts your spirit at rest, and you finally begin to accept the reality of the situation."},
      {speaker: "Mothman", dialogue: "\"Welcome to CLOWN MATH, the academy for cryptids to learn how to become human! I hope you'll enjoy your time here as we begin to nuture the lives of education of our students.\""},
      {speaker: "", dialogue: "You have a feeling you will enjoy your time here as you get to know Mothman and the rest of the faculty ;)"},
      {speaker: "Dev Team", dialogue: "Thanks for trying out the demo for Cryptids Dating Simulator! If you want to know about when we release our chapter updates, please sign up for our newsletter at the bottom of our homepage! (The next page will open our website on a new tab). Thank you again for playing! "},
      ]

    // Reset vars if the users decided to act like goofy goobers
    index = 0
    numberOfLines = lines.length
    console.log("---Number of Lines Imported: " + numberOfLines)

    // Set first line of dialogue and speaker as the help info
    // TODO


    // Get AFrame entities by ids that we are going to update
    dialogueTextEl = document.querySelector('#dialogueText')
    speakerPlateEl = document.querySelector('#speakerPlate');
    speakerTextEl = document.querySelector('#speakerText');

    // this.backgroundEl = document.querySelector('#background');
    // var buttonEls = document.querySelectorAll('.menu-button');
    // this.movieImageEl;
    // this.movieTitleEl = document.querySelector('#movieTitle');
    // this.movieDescriptionEl = document.querySelector('#movieDescription');

    // this.onMenuButtonClick = this.onMenuButtonClick.bind(this);
    // for (var i = 0; i < buttonEls.length; ++i) {
    //   buttonEls[i].addEventListener('click', this.onMenuButtonClick);
    // }
  },

  // onMenuButtonClick: function (evt) {
    // var movieInfo = this.movieInfo[evt.currentTarget.id];

    // this.backgroundEl.object3D.scale.set(1, 1, 1);

    // this.el.object3D.scale.set(1, 1, 1);
    // if (AFRAME.utils.device.isMobile()) { this.el.object3D.scale.set(1.4, 1.4, 1.4); }
    // this.el.object3D.visible = true;
    // this.fadeBackgroundEl.object3D.visible = true;

    // if (this.movieImageEl) { this.movieImageEl.object3D.visible = false; }
    // this.movieImageEl = movieInfo.imgEl;
    // this.movieImageEl.object3D.visible = true;

    // this.movieTitleEl.setAttribute('text', 'value', movieInfo.title);
    // this.movieDescriptionEl.setAttribute('text', 'value', movieInfo.description);
  // },

  onKeydown: function (evt) {
    let code = evt.code

    // (Help) Open/close the help menu
    // TODO: Create Help Menu
    // TODO: Add logic to flip help menu visibiliy 
    if (code === 'KeyH') { 
      console.log("Opening/closing help menu")
    }

    // (Next) Go to next line of dialogue ()
    if (code === 'KeyN') { 
      // Do not go over maximum amount of lines 
      if ((index + 1) <= numberOfLines){
        index++
      }

      // Hide speaker plate if character is monologuing
      let speaker = getSpeaker(index)
      setSpeakerPlateVisibility(speaker)

      // Set text values for speaker and dialogue
      speakerTextEl.setAttribute('text', 'value', speaker);
      dialogueTextEl.setAttribute('text', 'value', getDialogue(index));
    }

    // (Back) Go to previous line of dialogue 
    if (code === 'KeyB') { 
      // Do not go before the first line of dialogue
      if ((index - 1) >= 0){
        index--
      }

      // Hide speaker plate if character is monologuing
      let speaker = getSpeaker(index)
      setSpeakerPlateVisibility(speaker)
 
      // Set text values for speaker and dialogue
      speakerTextEl.setAttribute('text', 'value', speaker);
      dialogueTextEl.setAttribute('text', 'value', getDialogue(index));
    }
  }
});
